package com.dyts_rs_filesuploader.webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.dyts_rs_filesuploader.FileModel;
import com.dyts_rs_filesuploader.Logger;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Retrofit {
    private Context context;

    private String baseURL;
    private String endPoint, endPointExtra = "";

    private HashMap<String, String> params = new HashMap<>();
    private HashMap<String, String> headerMap = new HashMap<>();

    private RequestBody bodyRequest;
    private List<MultipartBody.Part> files = new ArrayList<>();
    private ProgressRequestBody.UploadCallbacks mUploadCallbacks;

    public Retrofit(Context context) {
        this.context = context;
    }

    /**
     * @param context
     * @return Instance of this class
     * create instance of this class
     */
    public static Retrofit with(Context context) {
        return new Retrofit(context);
    }

    /**
     * @param baseUrl
     * @return Instance
     * set Base Url for temporary
     * optional method if you set default Base URL in APIs class
     */
    public Retrofit setUrl(String baseUrl) {
        this.baseURL = baseUrl;
        return this;
    }

    /**
     * @param endPoint
     * @return Instance
     * set Endpoint when call every time
     */
    public Retrofit setAPI(String endPoint) {
        this.endPoint = endPoint;
        Log.e("URL", APIs.BASE_DOMAIN + endPoint);
        return this;
    }

    /**
     * @param token
     * @return Instance
     * set Header when call every time
     */
    public Retrofit setHeader(String token) {
        headerMap.put("Authorization", token);
        Log.e("header", token);
        return this;
    }


    /**
     * @param headerMap
     * @return Instance
     * set Header when call every time
     */
    public Retrofit setHeader(HashMap<String, String> headerMap) {
        this.headerMap = headerMap;
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            Log.e("header", entry.getKey() + "\t" + entry.getValue());
        }
        return this;
    }

    /**
     * @param mListener
     * @return Instance
     * set Endpoint when call every time
     */
    public Retrofit setMediaFileUploadListener(ProgressRequestBody.UploadCallbacks mListener) {
        this.mUploadCallbacks = mListener;
        return this;
    }

    /**
     * @param params
     * @return Call
     * to set request parameter
     */
    /*public Retrofit setGetParameters(HashMap<String, String> params) {
        if (params != null && !params.isEmpty()) {
            params.put("languageCode", new SessionManager(context).getDataByKey(SessionManager.KEY_LANGUAGE, AppConstants.EN).toUpperCase());
            for (Map.Entry<String, String> entry : params.entrySet()) {
                Log.e("params", entry.getKey() + "\t" + entry.getValue());
                endPointExtra = endPointExtra.concat(endPointExtra.contains("?") ? "&" : "?").concat(entry.getKey()).concat("=").concat(entry.getValue());
            }
            Log.e("EndpointExtra: ", endPointExtra);
        }
        return this;
    }*/
    public Retrofit setCustomGetParameters(HashMap<String, String> params) {
        if (params != null && !params.isEmpty()) {
            endPointExtra = endPointExtra.concat("json");
            for (Map.Entry<String, String> entry : params.entrySet()) {
                Log.e("params", entry.getKey() + "\t" + entry.getValue());
                endPointExtra = endPointExtra.concat(endPointExtra.contains("?") ? "&" : "?").concat(entry.getKey()).concat("=").concat(entry.getValue());
            }
            Log.e("EndpointExtra: ", endPointExtra);
        }
        return this;
    }

    /**
     * @param params
     * @return Call
     * to set request parameter
     */
    /*public Retrofit setParameters(HashMap<String, String> params) {
        this.params = params;
        params.put("languageCode", new SessionManager(context).getDataByKey(SessionManager.KEY_LANGUAGE, AppConstants.EN).toUpperCase());
        for (Map.Entry<String, String> entry : params.entrySet()) {
            Log.e("params", entry.getKey() + "\t" + entry.getValue());
        }
        return this;
    }*/

    /**
     * @param params
     * @return Call
     * to set request parameter
     */
    public Retrofit setParameters(HashMap<String, String> params, String langCode) {
        this.params = params;
        params.put("languageCode", langCode);
        for (Map.Entry<String, String> entry : params.entrySet()) {
            Logger.e("params", entry.getKey() + "\t" + entry.getValue());
        }
        return this;
    }

    /**
     * @param params
     * @return Call
     * to set request parameter
     */
    public Retrofit setFileParameters(HashMap<String, String> params, HashMap<FileModel, File> fileParams, String langCode) {
        files = new ArrayList<>();
        this.params = params;
        this.params.put("languageCode", langCode);

        Log.e("Error", String.valueOf(fileParams.size()));

        MultipartBody.Part body;
        for (Map.Entry<FileModel, File> entry : fileParams.entrySet()) {
            String fileName = entry.getKey().getName();
            Log.e("FileName", fileName);
            Log.e("FileKey", entry.getKey().getKey());

            if (entry.getKey().getType().equals(FileModel.MediaType.MEDIA_TYPE_VIDEO)) {
                Log.e("FileType Video", entry.getKey().getType());
//                body = MultipartBody.Part.createFormData(entry.getKey().getKey(), fileName, RequestBody.create(MediaType.parse("video/*"), entry.getValue()));
                body = MultipartBody.Part.createFormData(entry.getKey().getKey(), fileName, new ProgressRequestBody(entry.getValue(), "video", mUploadCallbacks));
            } else {
                Log.e("FileType Image", entry.getKey().getType());
                body = MultipartBody.Part.createFormData(entry.getKey().getKey(), fileName, RequestBody.create(MediaType.parse("image/*"), entry.getValue()));
            }
            files.add(body);
        }
        return this;
    }

    private ApiInterface getAPIInterface(OkHttpClient client) {
        return new retrofit2.Retrofit.Builder().baseUrl(baseURL != null ? baseURL : APIs.BASE_URL).client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);
    }

    public void setCallBackListener(JSONCallback listener) {
        makeCall().enqueue(listener);
    }

    public void setCustomCallBackListener(JSONCallback listener) {
        makeCustomCall().enqueue(listener);
    }


    private Call<ResponseBody> makeCall() {
        final SharedPreferences pref = context.getSharedPreferences(APIs.PREF_NAME, 0);
        Call<ResponseBody> call;

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request request;
                        if (pref.getBoolean(APIs.IS_LOG_IN, false)) {
                            request = chain.request().newBuilder().header("Authorization", pref.getString(APIs.AUTH_TOKEN, "")).method(original.method(), original.body()).build();
                        } else {
                            request = chain.request().newBuilder().method(original.method(), original.body()).build();
                        }
                        return chain.proceed(request);
                    }
                }).build();

        ApiInterface APIInterface = new retrofit2.Retrofit.Builder().baseUrl(baseURL != null ? baseURL : APIs.BASE_URL).client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);

        if (bodyRequest != null) {
            call = APIInterface.callPostMethod(endPoint, bodyRequest);
        } else if (params.size() > 0) {
            call = APIInterface.callPostMethod(endPoint, params);
        } else {
            call = APIInterface.callGetMethod(endPoint.concat(endPointExtra));
        }
        return call;
    }

    private Call<ResponseBody> makeCustomCall() {
        final SharedPreferences pref = context.getSharedPreferences(APIs.PREF_NAME, 0);
        Call<ResponseBody> call;

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request request;
                        if (pref.getBoolean(APIs.IS_LOG_IN, false)) {
                            request = chain.request().newBuilder().header("Authorization", pref.getString(APIs.AUTH_TOKEN, "")).method(original.method(), original.body()).build();
                        } else {
                            request = chain.request().newBuilder().method(original.method(), original.body()).build();
                        }
                        return chain.proceed(request);
                    }
                }).build();

        ApiInterface APIInterface = new retrofit2.Retrofit.Builder().baseUrl(baseURL != null ? baseURL : "").client(client).addCallAdapterFactory(RxJavaCallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).build().create(ApiInterface.class);

        if (bodyRequest != null) {
            call = APIInterface.callPostMethod(baseURL, bodyRequest);
        } else if (params.size() > 0) {
            call = APIInterface.callPostMethod(baseURL, params);
        } else {
            call = APIInterface.callGetMethod(baseURL.concat(endPointExtra));
        }
        return call;
    }

    public void setCallBackListenerMultipart(JSONCallbackMultipart listener) {
        setCallBackListenerMultipart(listener, "");
    }

    public void setCallBackListenerMultipart(JSONCallbackMultipart listener, String tag) {
        try {
            makeCallMultipart(tag).enqueue(listener);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Call<ResponseBody> makeCallMultipart(final String tag) {
        Log.e("Retrofit Tag: ", "==> " + tag);
        final SharedPreferences pref = context.getSharedPreferences(APIs.PREF_NAME, 0);
        Call<ResponseBody> call;

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10000, TimeUnit.SECONDS)
                .readTimeout(10000, TimeUnit.SECONDS)
                .writeTimeout(10000, TimeUnit.SECONDS)
//                .addInterceptor(provideHttpLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request request;
                        if (pref.getBoolean(APIs.IS_LOG_IN, false)) {
                            request = chain.request().newBuilder().header("Authorization", pref.getString(APIs.AUTH_TOKEN, "")).method(original.method(), original.body()).tag(tag).build();
                        } else {
                            request = chain.request().newBuilder().method(original.method(), original.body()).tag(tag).build();
                        }
                        return chain.proceed(request);
                    }
                }).build();

        ApiInterface APIInterface = new retrofit2.Retrofit.Builder()
                .baseUrl(baseURL != null ? baseURL : APIs.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(new GsonStringConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(ApiInterface.class);

        call = APIInterface.callPostMethodMultipart(endPoint, params, files);
        return call;
    }

    private static HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("Log", message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }
}
