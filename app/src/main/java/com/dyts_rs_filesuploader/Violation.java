package com.dyts_rs_filesuploader;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Violation {


    @SerializedName("violationLatitude")
    private String violationLatitude;
    @SerializedName("vehicleNo")
    private String vehicleNo;
    @SerializedName("violationDateTime")
    private String violationDateTime;
    @SerializedName("violationLongitude")
    private String violationLongitude;
    @SerializedName("reasonId")
    private String reasonId;
    @SerializedName("userId")
    private String userId;
    @SerializedName("languageCode")
    private String languageCode;
    @SerializedName("Authorization")
    private String Authorization;
    @SerializedName("Files")
    private List<FilesBean> Files;

    public String getViolationLatitude() {
        return violationLatitude;
    }

    public void setViolationLatitude(String violationLatitude) {
        this.violationLatitude = violationLatitude;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getViolationDateTime() {
        return violationDateTime;
    }

    public void setViolationDateTime(String violationDateTime) {
        this.violationDateTime = violationDateTime;
    }

    public String getViolationLongitude() {
        return violationLongitude;
    }

    public void setViolationLongitude(String violationLongitude) {
        this.violationLongitude = violationLongitude;
    }

    public String getReasonId() {
        return reasonId;
    }

    public void setReasonId(String reasonId) {
        this.reasonId = reasonId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getAuthorization() {
        return Authorization;
    }

    public void setAuthorization(String Authorization) {
        this.Authorization = Authorization;
    }

    public List<FilesBean> getFiles() {
        return Files;
    }

    public void setFiles(List<FilesBean> Files) {
        this.Files = Files;
    }

    public static class FilesBean {

        @SerializedName("Name")
        private String Name;
        @SerializedName("Key")
        private String Key;
        @SerializedName("Path")
        private String Path;
        @SerializedName("Type")
        private String Type;

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getKey() {
            return Key;
        }

        public void setKey(String Key) {
            this.Key = Key;
        }

        public String getPath() {
            return Path;
        }

        public void setPath(String Path) {
            this.Path = Path;
        }

        public String getType() {
            return Type;
        }

        public void setType(String Type) {
            this.Type = Type;
        }
    }
}
