package com.dyts_rs_filesuploader;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;


public class BootReceiver extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        try {
           // startJobScheduler();
            Intent intent2 = new Intent(context, ForegroundService.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent2);
            } else {
                context.startService(new Intent(intent2));
            }
            //context.bindService(intent2, mConnection, Context.BIND_AUTO_CREATE);
            IBinder binder = peekService(context, new Intent(context, ForegroundService.class));
            if (binder != null) {
                ((ForegroundService.LocalBinder) binder).getService();
                //.... other code here
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startJobScheduler() {
        ComponentName serviceComponent = new ComponentName(context, MyJobService.class);
        JobInfo.Builder jobInfo = new JobInfo.Builder(0, serviceComponent);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            jobInfo.setPeriodic(60000 * 5);
        } else {
            jobInfo.setPeriodic(60000 * 15);
        }
        jobInfo.setPersisted(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            jobInfo.setRequiresBatteryNotLow(true);
        }
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(jobInfo.build());
    }

}